# Docker environment for luminosity analyses

## Update the docker image

1. Commit (merge) and wait for the pipeline to finish.
2. Verify the image pushed by the pipeline (tagged with the commit sha).
3. Create a tag in the repository, which would push a correspondingly
   tagged image to the registry.

## Update image manually

To manually tag image `<id>` with `<tag>` and push it, do this
```shell
docker login gitlab-registry.cern.ch
docker tag <id> gitlab-registry.cern.ch/rmatev/renv:<tag>
docker push gitlab-registry.cern.ch/rmatev/renv:<tag>
docker logout gitlab-registry.cern.ch
```

## TODO

- list things expected/provided by container
  - kerberos, xrootd, root, strace (for fabricate) ...
